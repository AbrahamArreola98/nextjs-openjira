import { EntriesState } from ".";
import { Entry } from "../../interfaces";

type EntriesActionType =
    | { type: "entry/fetchEntries"; payload: Entry[] }
    | { type: "entry/addEntry"; payload: Entry }
    | { type: "entry/updateEntry"; payload: Entry };

export const entriesReducer = (state: EntriesState, action: EntriesActionType): EntriesState => {
    switch (action.type) {
        case "entry/fetchEntries":
            return {
                ...state,
                entries: [...action.payload],
            };

        case "entry/addEntry":
            return {
                ...state,
                entries: [...state.entries, action.payload],
            };

        case "entry/updateEntry":
            return {
                ...state,
                entries: state.entries.map((entry) => {
                    const { _id, status, description } = action.payload;

                    if (entry._id === _id) {
                        entry.status = status;
                        entry.description = description;
                    }

                    return entry;
                }),
            };

        default:
            return state;
    }
};
