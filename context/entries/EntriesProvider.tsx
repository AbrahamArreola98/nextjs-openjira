import { FC, ReactNode, useEffect, useMemo, useReducer } from "react";
import { useSnackbar } from "notistack";
import { EntriesContext, ContextProps, entriesReducer } from ".";
import { Entry } from "../../interfaces";
import { entriesApi } from "../../apis";

export interface EntriesState {
    entries: Entry[];
}

interface EntriesProviderProps {
    children: ReactNode;
}

const ENTRIES_INITIAL_STATE: EntriesState = {
    entries: [],
};

export const EntriesProvider: FC<EntriesProviderProps> = ({ children }) => {
    const [state, dispatch] = useReducer(entriesReducer, ENTRIES_INITIAL_STATE);

    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        fetchEntries();
    }, []);

    const fetchEntries = async () => {
        try {
            const { data } = await entriesApi.get<Entry[]>("/entries");
            dispatch({ type: "entry/fetchEntries", payload: data });
        } catch (error) {
            console.log(error);
        }
    };

    const addNewEntry = async (description: string) => {
        try {
            const { data } = await entriesApi.post<Entry>("/entries", { description });
            dispatch({ type: "entry/addEntry", payload: data });
        } catch (error) {
            console.log(error);
        }
    };

    const updateEntry = async (entry: Entry, showSnackBar = false) => {
        try {
            const { data } = await entriesApi.put<Entry>(`/entries/${entry._id}`, entry);

            dispatch({ type: "entry/updateEntry", payload: data });

            if (showSnackBar) {
                enqueueSnackbar("Entry successfully updated", {
                    variant: "success",
                    autoHideDuration: 1500,
                    anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                    },
                });
            }
        } catch (error) {
            console.log(error);
        }
    };

    const memoState: ContextProps = useMemo(
        () => ({
            ...state,
            addNewEntry,
            updateEntry,
        }),
        [state]
    );

    return <EntriesContext.Provider value={memoState}>{children}</EntriesContext.Provider>;
};
