import { UIState } from "./";

type UIActionType =
    | { type: "ui/openSidebar" }
    | { type: "ui/closeSidebar" }
    | { type: "ui/setAddingEntry"; payload: boolean }
    | { type: "ui/startDragging" }
    | { type: "ui/engDragging" };

export const UIReducer = (state: UIState, action: UIActionType): UIState => {
    switch (action.type) {
        case "ui/openSidebar":
            return {
                ...state,
                sideMenuOpen: true,
            };

        case "ui/closeSidebar":
            return {
                ...state,
                sideMenuOpen: false,
            };

        case "ui/setAddingEntry":
            return {
                ...state,
                isAddingEntry: action.payload,
            };

        case "ui/startDragging":
            return {
                ...state,
                isDragging: true,
            };

        case "ui/engDragging":
            return {
                ...state,
                isDragging: false,
            };

        default:
            return state;
    }
};
