import { FC, ReactNode, useMemo, useReducer } from "react";
import { UIContext, ContextProps, UIReducer } from "./";

export interface UIState {
    sideMenuOpen: boolean;
    isAddingEntry: boolean;
    isDragging: boolean;
}

interface UIProviderProps {
    children: ReactNode;
}

const UI_INITIAL_STATE: UIState = {
    sideMenuOpen: false,
    isAddingEntry: false,
    isDragging: false,
};

export const UIProvider: FC<UIProviderProps> = ({ children }) => {
    const [state, dispatch] = useReducer(UIReducer, UI_INITIAL_STATE);

    const openSideMenu = () => dispatch({ type: "ui/openSidebar" });
    const closeSideMenu = () => dispatch({ type: "ui/closeSidebar" });
    const setIsAddingEntry = (isAddingEntry: boolean) =>
        dispatch({ type: "ui/setAddingEntry", payload: isAddingEntry });
    const startDragging = () => dispatch({ type: "ui/startDragging" });
    const endDragging = () => dispatch({ type: "ui/engDragging" });

    const memoState: ContextProps = useMemo(
        () => ({
            ...state,
            openSideMenu,
            closeSideMenu,
            setIsAddingEntry,
            startDragging,
            endDragging,
        }),
        [state]
    );

    return <UIContext.Provider value={memoState}>{children}</UIContext.Provider>;
};
