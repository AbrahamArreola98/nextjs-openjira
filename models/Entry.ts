import mongoose, { Model, Schema } from "mongoose";
import { Entry } from "../interfaces";

export interface APIEntry extends Entry {}

const entrySchema = new Schema({
    description: { type: String, required: true },
    createdAt: { type: Number },
    status: {
        type: String,
        enum: {
            values: ["pending", "inProgress", "completed"],
            message: "{VALUE} is not a valid value",
        },
        default: "pending",
    },
});

const EntryModel: Model<APIEntry> = mongoose.models.Entry || mongoose.model("Entry", entrySchema);

export default EntryModel;
