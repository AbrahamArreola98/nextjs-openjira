interface SeedData {
    entries: SeedEntry[];
}

interface SeedEntry {
    description: string;
    status: string;
    createdAt: number;
}

export const seedData: SeedData = {
    entries: [
        {
            description: "Pending: Lorem ipsum dolor sit amet consectetur adipisicing elit.",
            status: "pending",
            createdAt: Date.now(),
        },
        {
            description: "In Progress: Lorem, ipsum dolor.",
            status: "inProgress",
            createdAt: Date.now() - 1000000,
        },
        {
            description:
                "Completed: Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos!",
            status: "completed",
            createdAt: Date.now() - 100000,
        },
    ],
};
