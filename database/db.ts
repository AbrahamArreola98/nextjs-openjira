import mongoose from "mongoose";

const MONGO_URL = process.env.MONGO_URL;
const NODE_ENV = process.env.NODE_ENV;

const mongoConnection = {
    isConnected: 0,
};

export const connect = async () => {
    if (mongoConnection.isConnected) {
        console.log("We're already connected!");
        return;
    }

    if (mongoose.connections.length > 0) {
        mongoConnection.isConnected = mongoose.connections[0].readyState;

        if (mongoConnection.isConnected === 1) {
            console.log("Using previous connection");
            return;
        }

        await mongoose.disconnect();
    }

    await mongoose.connect(MONGO_URL || "");
    mongoConnection.isConnected = 1;

    console.log(`Connected to mongoDB: ${MONGO_URL}`);
};

export const disconnect = async () => {
    if (NODE_ENV === "development") return;
    if (mongoConnection.isConnected === 0) return;

    await mongoose.disconnect();
    mongoConnection.isConnected = 0;

    console.log("Disconnected from mongoDB");
};
