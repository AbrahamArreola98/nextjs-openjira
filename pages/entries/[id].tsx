import React, { ChangeEvent, FC, useContext, useState } from "react";
import { GetServerSideProps } from "next";
import {
    Button,
    capitalize,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    FormControl,
    FormControlLabel,
    FormLabel,
    Grid,
    IconButton,
    Radio,
    RadioGroup,
    TextField,
} from "@mui/material";
import { Layout } from "../../components/layouts";
import SaveOutlinedIcon from "@mui/icons-material/SaveOutlined";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import { Entry, EntryStatus } from "../../interfaces";
import { services } from "../../services";
import { EntriesContext } from "../../context/entries";
import { dateFunctions } from "../../utils";

const validStatus: EntryStatus[] = ["pending", "inProgress", "completed"];

interface Props {
    entry: Entry;
}

const EntryPage: FC<Props> = ({ entry }) => {
    const [inputValue, setInputValue] = useState(entry.description);
    const [status, setStatus] = useState<EntryStatus>(entry.status);
    const [touched, setTouched] = useState(false);

    const isNotValidEntryTitle = inputValue.length <= 0 && touched;

    const { updateEntry } = useContext(EntriesContext);

    const onTextFieldChange = (e: ChangeEvent<HTMLInputElement>) => {
        setInputValue(e.target.value);
    };

    const onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
        setStatus(e.target.value as EntryStatus);
    };

    const onSave = () => {
        const updatedEntry: Entry = {
            ...entry,
            status,
            description: inputValue,
        };

        updateEntry(updatedEntry, true);
    };

    return (
        <Layout title={inputValue || "Entry Page"}>
            <Grid container justifyContent="center" sx={{ marginTop: 2 }}>
                <Grid item xs={12} sm={8} md={6}>
                    <Card sx={{ position: "relative" }}>
                        <IconButton
                            sx={{
                                position: "absolute",
                                top: 12,
                                right: 12,
                                backgroundColor: "error.dark",
                            }}
                        >
                            <DeleteOutlinedIcon />
                        </IconButton>
                        <CardHeader
                            title={`Entry: ${inputValue}`}
                            subheader={`Created ${dateFunctions.getFormatDistanceToNow(
                                entry.createdAt
                            )} ago`}
                            sx={{ marginRight: 6 }}
                        />
                        <CardContent>
                            <TextField
                                sx={{ marginTop: 2, marginBottom: 1 }}
                                fullWidth
                                placeholder="New Entry"
                                autoFocus
                                multiline
                                label="New Entry"
                                value={inputValue}
                                onChange={onTextFieldChange}
                                onBlur={() => setTouched(true)}
                                helperText={isNotValidEntryTitle && "Type entry's tittle"}
                                error={isNotValidEntryTitle}
                            />

                            <FormControl>
                                <FormLabel>Status:</FormLabel>
                                <RadioGroup row value={status} onChange={onStatusChange}>
                                    {validStatus.map((status) => (
                                        <FormControlLabel
                                            key={status}
                                            value={status}
                                            control={<Radio />}
                                            label={capitalize(status)}
                                        />
                                    ))}
                                </RadioGroup>
                            </FormControl>
                        </CardContent>
                        <CardActions>
                            <Button
                                startIcon={<SaveOutlinedIcon />}
                                variant="contained"
                                fullWidth
                                onClick={onSave}
                                disabled={inputValue.length <= 0}
                            >
                                Save
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </Layout>
    );
};

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
    const { id } = params as { id: string };

    const entry = await services.getEntryById(id);

    if (!entry) {
        return {
            redirect: {
                destination: "/",
                permanent: false,
            },
        };
    }

    return {
        props: {
            entry,
        },
    };
};

export default EntryPage;
