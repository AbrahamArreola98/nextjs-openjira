import type { NextApiRequest, NextApiResponse } from "next";
import { db } from "../../../database";
import { APIEntry, Entry } from "../../../models";

type Data = { message: string } | APIEntry | APIEntry[];

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
    switch (req.method) {
        case "GET":
            return getEntries(res);

        case "POST":
            return createEntry(req, res);

        default:
            return res.status(404).json({ message: "Route not found" });
    }
}

const getEntries = async (res: NextApiResponse<Data>) => {
    await db.connect();
    const entries = await Entry.find().sort({ createdAt: "ascending" });
    await db.disconnect();

    res.status(201).json(entries);
};

const createEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
    const { description = "" } = req.body;

    const newEntry = new Entry({ description, createdAt: Date.now() });

    try {
        await db.connect();
        await newEntry.save();
        await db.disconnect();

        return res.status(201).json(newEntry);
    } catch (err) {
        await db.disconnect();
        console.log(err);

        return res
            .status(500)
            .json({ message: "Something went wrong when trying to create a new entry" });
    }
};
