import type { NextApiRequest, NextApiResponse } from "next";
import mongoose from "mongoose";
import { db } from "../../../database";
import { APIEntry, Entry } from "../../../models";

type Data = { message: string } | APIEntry;

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
    switch (req.method) {
        case "GET":
            return getEntry(req, res);

        case "PUT":
            return updateEntry(req, res);

        default:
            return res.status(404).json({ message: "Route not found" });
    }
}

const getEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
    const { id } = req.query;

    if (!mongoose.isValidObjectId(id)) {
        return res.status(400).json({ message: `Entry ID: ${id} is not valid` });
    }

    await db.connect();

    const foundEntry = await Entry.findById(id);

    if (!foundEntry) {
        await db.disconnect();
        return res.status(400).json({ message: `Record with such ID was not found` });
    }

    res.status(200).json(foundEntry);
};

const updateEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
    const { id } = req.query;

    if (!mongoose.isValidObjectId(id)) {
        return res.status(400).json({ message: `Entry ID: ${id} is not valid` });
    }

    await db.connect();

    const entryToUpdate = await Entry.findById(id);

    if (!entryToUpdate) {
        await db.disconnect();
        return res.status(400).json({ message: `Record with such ID was not found` });
    }

    const { description = entryToUpdate.description, status = entryToUpdate.status } = req.body;

    try {
        const updatedEntry = await Entry.findByIdAndUpdate(
            id,
            { description, status },
            { runValidators: true, new: true }
        );

        res.status(200).json(updatedEntry!);
    } catch (error: any) {
        await db.disconnect();
        res.status(400).json({ message: error.errors.status.message });
    }
};
