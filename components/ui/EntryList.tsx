import React, { FC, useContext, useMemo, DragEvent } from "react";
import { List, Paper } from "@mui/material";
import { EntryCard } from ".";
import { EntryStatus } from "../../interfaces";
import { EntriesContext } from "../../context/entries";
import { UIContext } from "../../context/ui";

import styles from "./entry-list.module.css";

interface Props {
    status: EntryStatus;
}

export const EntryList: FC<Props> = ({ status }) => {
    const { entries, updateEntry } = useContext(EntriesContext);
    const { isDragging, endDragging } = useContext(UIContext);

    const entriesByStatus = useMemo(
        () => entries.filter((entry) => entry.status === status),
        [entries, status]
    );

    const allowDrop = (event: DragEvent<HTMLDivElement>) => {
        event.preventDefault();
    };

    const onDropEntry = (event: DragEvent<HTMLDivElement>) => {
        const id = event.dataTransfer.getData("text");

        const entry = entries.find((entry) => entry._id === id)!;

        updateEntry({ ...entry, status });
        endDragging();
    };

    return (
        <div
            onDragOver={allowDrop}
            onDrop={onDropEntry}
            className={isDragging ? styles.dragging : ""}
        >
            <Paper
                sx={{
                    height: "calc(100vh - 180px)",
                    overflow: "scroll",
                    backgroundColor: "transparent",
                    padding: "1px 5px",
                }}
            >
                <List sx={{ opacity: isDragging ? 0.4 : 1, transition: "all .3s ease-in-out" }}>
                    {entriesByStatus.map((entry) => (
                        <EntryCard key={entry._id} entry={entry} />
                    ))}
                </List>
            </Paper>
        </div>
    );
};
